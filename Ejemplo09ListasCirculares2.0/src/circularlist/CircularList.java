package circularlist;

import java.util.Iterator;

import node.node;

public class CircularList<T> implements Iterable<T>{

	private node<T> sentinel = null;
	private node<T> actual = null;
	
	public CircularList() {
		sentinel = new node<T>();
		actual   = new node<T>();
		sentinel.setIndex(-1);
		actual.setIndex(-1);
	}
	
	public CircularList(T value) {
		this();
		sentinel.setNext(new node<T>(value));
		actual = sentinel.getNext();
		sentinel.getNext().setNext(actual);
	}

/*INICIO RE-INDEX*/
	public void reindex() {
		node<T> tmp = sentinel.getNext();
		int inde = 1;
		if(!isEmpty()){
			while (!tmp.getNext().equals(sentinel.getNext())) {
				tmp.setIndex(inde);
				tmp = tmp.getNext();
				inde++;
			}
			tmp.setIndex(inde);
		}
	}
/*FIN RE-INDEX*/
	
/*INICIO ADDFIRST*/
	public void addFirst(T value) {
		node<T> nuevo = new node<T>(value);
		node<T> last = getLast();
		
		if(isEmpty()){
			sentinel.setNext(nuevo);
			nuevo.setNext(nuevo);
		}else{
			nuevo.setNext(sentinel.getNext());
			sentinel.setNext(nuevo);
			last.setNext(nuevo);
		}
		reindex();
	}
/*FIN ADDFIRST*/

/*INICIO ADDLAST*/
	public void addLast(T value) {
		node<T> tmp = sentinel.getNext();
		node<T> nuevo = new node<T>(value);
		
		if(isEmpty()){
			sentinel.setNext(nuevo);
			nuevo.setNext(nuevo);
		}else{
			while (!tmp.getNext().equals(sentinel.getNext())) {
				tmp = tmp.getNext();
			}
			tmp.setNext(nuevo);
			nuevo.setNext(sentinel.getNext());
		}
		reindex();
	}
/*FIN ADDLAST*/
	
/*INICIO ADDBEFORE*/
	public void addBefore(T value, T newvalue) {
		if(isEmpty()){
			System.out.println("No hay datos para realizar accion");
		}else{
			node<T> nuevo = new node<T>(newvalue);
			node<T> last = getLast();
			node<T> found = SearchBefore(value, sentinel.getNext());
			if(found.getNext().equals(sentinel.getNext())) {
				nuevo.setNext(sentinel.getNext());
				last.setNext(nuevo);
				sentinel.setNext(nuevo);
			}else {
				nuevo.setNext(found.getNext());
				found.setNext(nuevo);
			}
		}
		reindex();
	}
/*FIN ADDBEFORE*/
	
/*INICIO ADDBAFTER*/
	public void addAfter(T value, T newvalue) {
		if(isEmpty()){
			System.out.println("No hay datos para realizar accion");
		}else{
			node<T> nuevo = new node<T>(newvalue);
			node<T> tmp = Search(value);
			nuevo.setNext(tmp.getNext());
			tmp.setNext(nuevo);
		}
		reindex();
	}
/*FIN ADDAFTER*/
	
/*INICIO GETLAST ADDFISRT*/
	private node<T> getLast(){
		node<T> tmp = sentinel.getNext();
		if(!isEmpty()){
			while(!sentinel.getNext().equals(tmp.getNext()))
				tmp = tmp.getNext();
			return tmp;
		}//end if
		return null;
	}
/*FIN GETLAST ADDFIRST*/
	
/*INICIO GETLAST*/
	public T getLastt(){
		node<T> tmp = sentinel.getNext();
		if(!isEmpty()){
			while(!sentinel.getNext().equals(tmp.getNext()))
				tmp = tmp.getNext();
			return tmp.getValue();
		}//end if
		return null;
	}
/*FIN GETLAST*/
	
/*INICIO GETFIST*/
	public T getFirstt(){
		node<T> tmp = sentinel.getNext();
		if(!isEmpty()){
			return tmp.getValue();
		}
		return null;
	}
/*FIN GETFIRST*/

/*INICIO REMOVEFIRST*/
	public void removefirst() {
		if(!isEmpty()) {
			T prim = getFirstt();
			node<T> tmp = SearchBefore(prim, sentinel.getNext());
			if(size() > 1) {
				sentinel.setNext(sentinel.getNext().getNext());
				tmp.setNext(tmp.getNext().getNext());
				reindex();
			}else {
				eliminar();
			}
		}
	}
/*FIN REMOVEFIRST*/
	
/*INICIO REMOVELAST*/
	public void removelast() {
		if(!isEmpty()) {
			T ulti = getLastt();
			node<T> tmp = SearchBefore(ulti, sentinel.getNext());
			if(size() > 1) {
				tmp.setNext(tmp.getNext().getNext());
				reindex();
			}else {
				eliminar();
			}
			
		}
	}
/*FIN REMOVELAST*/
	
/*INICIO REMOVEAFTER*/
	public void removeafter(T value) {
		if(!isEmpty()) {
			node<T> found = Search(value);
			node<T> tmp = sentinel;
			if(found != null) {
				if(tmp.getNext().equals(found.getNext())) {
					//System.out.println("ENTRO");
					found.setNext(found.getNext().getNext());
					tmp.setNext(tmp.getNext().getNext());
					reindex();
				}else {
					//System.out.println("entro");
					found.setNext(found.getNext().getNext());
					reindex();
				}
			}
		}
	}
/*FIN REMOVEAFTER*/
	
/*INICIO REMOVEBEFORE*/
	public void removebefore(T value) {
		if(!isEmpty()) {
			node<T> found = Search(value);
			T value2 = found.getValue();
			node<T> found2 = SearchBefore(value2, sentinel.getNext());
			T value3 = found2.getValue();
			node<T> found3 = SearchBefore(value3, sentinel.getNext());
			node<T> tmp = sentinel;
			
			if(found != null) {
				if(tmp.getNext().equals(found3.getNext())) {
					//System.out.println("ENTRO");
					found3.setNext(found3.getNext().getNext());
					tmp.setNext(tmp.getNext().getNext());
					reindex();
				}else {
					//System.out.println("entro");
					found3.setNext(found3.getNext().getNext());
					reindex();
				}
			}
		}
	}
/*FIN REMOVEBEFORE*/
	
/*INICIO REMOVE*/
	public boolean remove(T value){
		if(!isEmpty()){
			node<T> found = Search(value);
			if(found != null){
				node<T> tmp = SearchBefore(value, sentinel.getNext());
				if(tmp.equals(tmp.getNext()))
					sentinel.setNext(null);
				else if(sentinel.getNext().equals(found)){
					 sentinel.setNext(found.getNext());
					 tmp.setNext(found.getNext());
				 }else{
					 tmp.setNext(found.getNext());
				 }
			}
		}
		reindex();
		return true;
	}
/*FIN REMOVE*/
	
/*INICIO REMPLAZAR*/
	public void replace(T value, T newvalue){
        if (Searchh(value)) {
        	node<T> tmp = sentinel;
            while(tmp.getValue() != value){
                tmp = tmp.getNext();
            }
            tmp.setValue(newvalue);
        }
    }
/*FIN REMPLAZAR*/
	
/*INICIO LIMPIAR*/
	public void eliminar(){
        sentinel.setNext(null);
        sentinel.setIndex(-1);
        actual.setValue(null);
        actual.setIndex(-1);
    }
/*FIN LIMPIAR*/
	
/*iNICIO INDEXOF*/
	public long indexof(T value){
		if(!isEmpty()) {
			return indexof(value, sentinel.getNext());
		}else {
			return -1;
		}
//		return (!isEmpty())?indexof(value, sentinel.getNext()):null;
	}
	private long indexof(T value, node<T> list){
		if(list.getNext().getValue().equals(value)){
			if(sentinel.getNext().getValue().equals(value)) {
				//System.out.println("ENTRO");
				return 1;
			}
			//System.out.println("entro");
			return list.getIndex()+1;
		}
		if(list.getNext().equals(sentinel.getNext())){
			return -1;
		}
		
		return indexof(value, list.getNext());
	}
/*FIN INDEXOF*/
	
/*INICIO BUSCAR ANTES*/
	private node<T> SearchBefore(T value, node<T> list){
		if(list.getNext().getValue().equals(value)){
			return list;
		}
		if(list.getNext().equals(sentinel.getNext())){
			return null;
		}
		
		return SearchBefore(value, list.getNext());
	}
/*FIN BUSCAR ANTES*/
	
/*INICIO BUSCAR*/
	public node<T> Search(T value){
		return (!isEmpty())?Search(value, sentinel.getNext()):null;
	}
	private node<T> Search(T value, node<T> list){
		if(list.getNext().getValue().equals(value)){
			return list.getNext();
		}
		if(list.getNext().equals(sentinel.getNext())){
			return null;
		}
		
		return Search(value, list.getNext());
	}
	public boolean Searchh(T value){
		return (!isEmpty())?Searchh(value, sentinel.getNext()):null;
	}
	private boolean Searchh(T value, node<T> list){
		if(list.getNext().getValue().equals(value)){
			return true;
		}
		if(list.getNext().equals(sentinel.getNext())){
			return false;
		}
		
		return Searchh(value, list.getNext());
	}
/*FIN BUSCAR*/
	
/*INICIO BACIA O NO*/
	public boolean isEmpty(){
		return (sentinel.getNext() == null)?true:false;
	}
/*FIN BACIA O NO*/
	
/*INICIO SIZE*/
	public long size() {
		node<T> tmp = sentinel.getNext();
		int size = 0;
		if(!isEmpty()){
			while (!tmp.getNext().equals(sentinel.getNext())) {
				tmp = tmp.getNext();
				size++;
			}
			size++;
		}
		return size;
	}
/*FIN SIZE*/
	
/*INICIO IMPRIMIR*/
	public void printer() {
		node<T> tmp = sentinel.getNext();
		if(!isEmpty()){
			while (!tmp.getNext().equals(sentinel.getNext())) {
				System.out.print(tmp.getIndex() + "� ");
				System.out.println(tmp.getValue());
				tmp = tmp.getNext();
			}
			System.out.print(tmp.getIndex() + "� ");
			System.out.println(tmp.getValue());
		}
	}
	public void pronter() {
		pronter(sentinel.getNext());
	}
	private void pronter(node<T> tmp) {
		if (tmp.getNext().equals(sentinel.getNext()))
			return;
		else{
			System.out.print(tmp.getIndex() + "� ");
			System.out.println(tmp.getValue());
		}
		pronter(tmp.getNext());
	}
/*FIN IMPRIMIR*/
	
/*INICIO ITERADOR*/
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			node<T> tmp = sentinel.getNext();
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				tmp = tmp.getNext();
				return (tmp != sentinel.getNext())?true:false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				return tmp.getValue();
			}
		};
	}
/*FIN ITERADOR*/
}
