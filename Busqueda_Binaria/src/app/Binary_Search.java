package app;

public class Binary_Search {
	
	public int Binaria(int dato, int[] arreglo, int inicio, int fin) {
		
		int pos = 0;
		
		if(inicio <= fin) {
			pos = (inicio + fin) / 2;
		}else {
			return -1;
		}
		if(arreglo[pos] == dato) {
			return pos+1;
		}
		if(arreglo[pos] < dato) {
			return Binaria(dato, arreglo, pos+1, fin);
		}else {
			return Binaria(dato , arreglo, inicio, pos-1);
		}
		
	}
	
}