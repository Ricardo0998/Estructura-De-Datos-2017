package app;

public class Pruebas {

	static int suma;
	static int cont = 0;
	
	static int Prueba1(int numero, int num){
		
		if(numero/10<1){
			return suma + numero;
		}else{
			suma=suma+numero%10;
			numero = numero/10;
			return Prueba1(suma, numero);
		}
	}
	
	static String Prueba2(int cont, int numero){
		if(cont == numero){
			return "" + cont;
		}else{
			return cont + "\n" + Prueba2(cont+1, numero);
		}
	}
	
	static int Prueba3(int num){
		if(num<10){
			return cont+1;
		}else{
			num = num/10;
			cont=cont+1;
			return Prueba3(num);
		}
	}
	
	static int Prueba4(int a, int b){
		if(b==1){
			return a;
		}else{
			return a + Prueba4(a,b-1);
		}
	}
	
	static int Prueba5(int numero, int potencia){
		if(potencia == 0){
			return 1;
		}else{
			return numero * Prueba5(numero, potencia-1);
		}
	}
	
	static double Prueba6(int b, int p){
		if(b==0){
			return 0;
		}else{
			return (b%10) * (Math.pow(2, p)) + Prueba6(b/10, ++p);
		}
	}
	
}
