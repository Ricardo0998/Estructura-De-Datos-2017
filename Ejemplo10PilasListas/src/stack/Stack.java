package stack;

import java.util.Iterator;

import node.node;

public class Stack<T extends Comparable <T> > implements iStack <T>, Iterable <T>{
	
	private int top   = -1;
	private int count = 0;
	private int tamano = 0;
	
	private node<T> start = null, end = null;
	
	public Stack(){
		start = new node<T>();
		start.setIndex(-1);
		end = new node<T>();
		end.setIndex(-1);
		tamano = 10;
	}
	
	public Stack(int tama�o){
		this();
		tamano = tama�o;
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			node<T> tmp = end;
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				tmp = tmp.getBack();
				return (tmp != null)?true:false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				return tmp.getValue();
			}
		};
	}
	
	@Override
	public T pop() throws StackEmptyException {
		if (isempty()) throw new StackEmptyException("Pila Vacia");
		node<T> tmp = end;
		T val = tmp.getBack().getValue();
        if (tmp.getBack() != null){
        	tmp.setBack(tmp.getBack().getBack());
        	tmp = tmp.getBack();
        	tmp.setNext(null);
        }
        count--;
		return val;
	}
	
	@Override
	public void push(T value) throws StackFullException {
		
		if (isfull()) throw new StackFullException("Pila Llena");
		node<T> tmp = end.getBack();
		end.setBack(new node<T>(value));
		
		if(tmp == null){
			start.setNext(end.getBack());
			end.getBack().setIndex(top);
		}else{
			end.getBack().setBack(tmp);
			tmp.setNext(end.getBack());
		}
		
		count++;
		
	}
	
	@Override
	public boolean isempty() {
		
		return (count == 0);
		
	}
	
	@Override
	public boolean isfull() {
		
		return (count == tamano);
		
	}
	
	@Override
	public T peek() throws StackEmptyException {
		if (isempty()) throw new StackEmptyException("Pila Vacia");
		node<T> tmp = end;
		tmp = tmp.getBack();
		T val = tmp.getValue();
		return val;
	}
	
	@Override
	public int size() {
		
		return count;
		
	}
	
	@Override
	public void clear() {

		start.setNext(null);
        start.setIndex(-1);
        end.setBack(null);
        end.setIndex(-1);
        top = -1;
        count = 0;
		
	}
	
	@Override
	public T search(T value) throws StackEmptyException {
		node<T> tmp = end;
		while(tmp.getBack()!=null){
			if(tmp.getBack().getValue().equals(value)){
				T val = tmp.getBack().getValue();
				return val;
			}else{
				tmp = tmp.getBack();
			}
		}
		return null;
	}
	
}
