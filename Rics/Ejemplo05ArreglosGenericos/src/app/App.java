package app;

public class App {

	public static void main(String[] args) {
		
		Bolsa<Double> enteros = new Bolsa<>(Double.class);
		
		//Bolsa papas = new Bolsa<>(Papas.class);
		//Bolsa<String> cadena = new Bolsa<>(String.class);
		
		enteros.add(6d);
		enteros.add(4.3);
		enteros.Imprimir();
		
		/*cadena.add("Anna");
		cadena.add("Luis");
		cadena.add("" + 4);*/
		
		People<Persona> gente = new People<>(Persona.class);
		
		gente.add(new Persona());
		gente.add(new Empleado());
		gente.add(new Alumno());
		
		gente.Imprimir();
		
	}
	
	public static <T> void swap(int ...is) {
		
	}

}
