package app;

import java.lang.reflect.Array;

public class Bolsa<T extends Number> {

	private T  [] data = null;
	private Class<T> type = null;
	private int cont = 0;
	
	public Bolsa(Class <T> type) {
		data = (T[])Array.newInstance(type, 10);
		this.type = type;
	}
	
	public Bolsa(Class <T> type, int size) {
		data = (T[])Array.newInstance(type, size);
		this.type = type;
	}
	
	public boolean add(T value) {
		
		
		if(cont < data.length) {
			data[cont++] = value;
			return true;
		}
		return false;
	}
	
	public void Imprimir() {
		for (int i = 0; i < cont; i++) {
			System.out.println(data[i].toString());
		}
	}
	
}