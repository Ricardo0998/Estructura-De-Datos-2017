package doublelinkedlist;

import java.util.ArrayList;
import java.util.Iterator;

import node.node;

public class DoubleLinkedList<T> implements Iterable<T>{
	
	private node<T> start = null, end = null;
	
	public DoubleLinkedList(){
		start = new node<T>();
		start.setIndex(-1); // <- INDEXANDO SENTINELA START
		end = new node<T>();
		end.setIndex(-1);
	}
	public DoubleLinkedList(T value){
		this();
		end.setBack(new node<T>(value));
		start.setNext(end.getBack());
		start.getNext().setIndex(-1); // <-- INDEXANDO NODO
	}

/*INICIO RE-INDEX*/
	public void reindex() {
		node<T> tmp = start;
		int inde = 0;
		while (tmp.getNext()!=null) {
			tmp = tmp.getNext();
			tmp.setIndex(inde);
			inde++;
		}
}
/*FIN RE-INDEX*/
	
/*INICIO INDEXOF*/
	public long indexof(T value){
		node<T> tmp = start;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
				return tmp.getIndex()+2;
			}else{
				tmp = tmp.getNext();
			}
		}
		
		return 0;
	}
/*FIN INDEXOF*/
	
/*INICIO ADDSTART*/
	public void addStart(T value){
		node<T> tmp = start.getNext();
		start.setNext(new node<T>(value));
		
		if(tmp == null){
			end.setBack(start.getNext());
			start.getNext();//.setIndex(0);
		}else{
			start.getNext().setNext(tmp);
			tmp.setBack(start.getNext());
		}
		reindex();
	}
/*FIN ADDSTART*/
	
/*INICIO ADDEND*/
	public void addEnd(T value){
		node<T> tmp = end.getBack();
		end.setBack(new node<T>(value));
		
		if(tmp == null){
			start.setNext(end.getBack());
			end.getBack().setIndex(0);
		}else{
			end.getBack().setBack(tmp);
			tmp.setNext(end.getBack());
		}
		reindex();
	}
/*FIN ADDEND*/
	
/*INICIO GETFIRST*/
	public void getfirst(){
		node<T> tmp = start;
        if (tmp.getNext()!=null){
        	tmp = tmp.getNext();
            System.out.println("Primero: " + tmp.getValue());
        }
    }
/*FIN GETFIRST*/

/*INICIO GETLAST*/
	public void getlast(){
		node<T> tmp = end;
        if (tmp.getBack()!=null){
        	tmp = tmp.getBack();
            System.out.println("Ultimo: " + tmp.getValue());
        }
    }
/*FIN GETLAST*/
	
/*INICIO REMOVEFIRST*/
	public void removefirst(){
		node<T> tmp = start;
        if (tmp.getNext() != null){
        	tmp.setNext(tmp.getNext().getNext());
        	tmp = tmp.getNext();
        	tmp.setBack(null);
        }
        reindex();
    }
/*FIN REMOVEFIRST*/
	
/*INICIO REMOVEFIRST*/
	public void removelast(){
		node<T> tmp = end;
        if (tmp.getBack() != null){
        	tmp.setBack(tmp.getBack().getBack());
        	tmp = tmp.getBack();
        	tmp.setNext(null);
        }
        reindex();
    }
/*FIN REMOVEFIRST*/

/*INICIO REMOVEAFTER*/
	public void removeafter(T value){
		try{node<T> tmp = start;
		while(tmp.getNext()!=null){
			if(tmp.getNext().getValue().equals(value)){
					tmp = tmp.getNext();
					tmp.setNext(tmp.getNext().getNext());
					tmp.getNext().setBack(tmp);
					reindex();
				//return;
			}else{
				tmp = tmp.getNext();
			}
			reindex();
		}
		}catch (Exception e) {
			System.out.println("Es el ultimo nodo");
		}
	}
/*FIN REMOVEAFTER*/
	
/*INICIO REMOVEBEFORE*/
	public void removebefore(T value){
		try{node<T> tmp = end;
		while(tmp.getBack()!=null){
			if(tmp.getBack().getValue().equals(value)){
					tmp = tmp.getBack();
					tmp.setBack(tmp.getBack().getBack());
					tmp.getBack().setNext(tmp);
					reindex();
				//return;
			}else{
				tmp = tmp.getBack();
			}
			reindex();
		}
		}catch (Exception e) {
			System.out.println("Es el ultimo nodo");
		}
	}
/*FIN REMOVEBEFORE*/
	
/*INICIO SIZE*/
	public int size() {
		node<T> tmp = start;
		int i = 0;
		while (tmp.getNext()!=null) {
			tmp = tmp.getNext();
			i++;
		}
		return i;
	}
/*FIN SIZE*/
	
/*INICIO BUSCAR*/
	public node<T> Searchr(T value){
		return Searchr(value, start, end);
	}
	private node<T> Searchr(T value, node<T> start, node<T> end){  // BUSCAR NODE
		if(start.getNext() == null || end.getBack() == null){
			System.out.println("Primer if"); return null;
		}else if(start.getNext().getValue().equals(value)){
				return start.getNext();
			}else if(end.getBack().getValue().equals(value)){
					return end.getBack();
				}else if ((start.getNext().equals(end)) || (start.equals(end))){
					System.out.println("Segundo if"); return null;
				}
			return Searchr(value, start.getNext(), end.getBack());
			}
	public boolean Search(T value){
		return Search(value, start, end);
	}
	private boolean Search(T value, node<T> start, node<T> end){  // Buscar boolean
		if(start.getNext() == null || end.getBack() == null){
			System.out.println("Primer if"); return false;
		}else if(start.getNext().getValue().equals(value)){
				return true;
			}else if(end.getBack().getValue().equals(value)){
					return true;
				}else if ((start.getNext().equals(end)) || (start.equals(end))){
					System.out.println("Segundo if"); return false;
				}
			return Search(value, start.getNext(), end.getBack());
			}
/*FIN DE BUSCAR*/

/*INICIO REMOVER*/
	public boolean remove(T value){
		node<T> tmp = Searchr(value);
		if(tmp!=null){
			if(tmp.getNext() != null){
				tmp.getNext().setBack(tmp.getBack());
			}else{
				end.setBack(tmp.getBack());
			}
			if(tmp.getBack() != null){
				tmp.getBack().setNext(tmp.getNext());
			}else{
				start.setNext(tmp.getNext());
			}
			reindex();
			return true;
		}
		return false;
	}
/*FIN REMOVER*/
	
/*INICIO REMPLACE*/
	public void replace(T value, T newvalue){
        if (Search(value)) {
        	node<T> tmp = start;
            while(tmp.getValue() != value){
                tmp = tmp.getNext();
            }
            tmp.setValue(newvalue);
        }
    }
/*FIN REPLACE*/
	
/*INICIO CHEQUEO*/
	public void isempty() {
		isempty(start, end);
	}
	private void isempty(node<T> listastart, node<T> listaend) {
		if(listastart.getNext() != null && listaend.getBack() != null) {
			//return true;
			System.out.println("Contiene elementos");
		}else {
			//return false;
			System.out.println("No hay elementos");
		}
	}
/*FIN CHEQUEO*/
	
/*INICIO CLEAR*/
	public void clear() {
		start.setNext(null);
        start.setIndex(-1);
        end.setBack(null);
        end.setIndex(-1);
	}
/*FIN CLEAR*/
	
/*INICIO IMPRIMIR*/
	public void pronterstart() { // IMPRIMIR INTERATIVO
		node<T> tmp = start;
		while (tmp.getNext()!=null) {
			tmp = tmp.getNext();
			System.out.print(tmp.getIndex()+1 + "� ");
			System.out.println(tmp.getValue());
		}
	}
	public void pronterend() { // IMPRIMIR INTERATIVO
		node<T> tmp = end;
		while (tmp.getBack()!=null) {
			tmp = tmp.getBack();
			System.out.print(tmp.getIndex()+1 + "� ");
			System.out.println(tmp.getValue());
		}
	}
	public void printer() { // IMPRIMIR RECURSIVO
		printer(start);
	}
	private void printer(node<T> tmp) {
		if (tmp.getNext() == null)
			return;
		else{
			System.out.println(tmp.getNext().getValue());
		    printer(tmp.getNext());
		}
	}
/*FIN DE IMPRIMIR*/
	
/*INICIO ITERADOR*/
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() {
			node<T> tmp = start;
			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				tmp = tmp.getNext();
				return (tmp != null)?true:false;
			}

			@Override
			public T next() {
				// TODO Auto-generated method stub
				return tmp.getValue();
			}
		};
	}
/*FIN ITERADOR*/
	
	public void list() {
		node<T> tmp = start;
		ArrayList<String> names = new ArrayList<String>();
		int i = 0;
		while (tmp.getNext()!=null) {
			tmp = tmp.getNext();
			String i2 = Integer.toString(i+1);
			names.add((String) i2 + "� " + tmp.getValue());
			i++;
		}

		for (String value : names) {
			System.out.println(value);
		}
	}
	
/* ---------------------------------------------------------------------------------------------- */
							/*METODOS PARA TAREA LUNES 6 DE NOVIEMBRE*/
	
	public boolean iguales (DoubleLinkedList<T> lista) { //Ejercisio N�1
		if (lista.size() == this.size())
			return iguales(this.start, lista.start);
		return false;
	}
	public boolean iguales(node<T> start ,node<T> lstart) {
		return false;
		
	}

}