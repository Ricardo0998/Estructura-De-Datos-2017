package app;

import linkedList.LinkedList;

public class App {

	public static void main(String[] args) {
		LinkedList<String> names = new LinkedList<String>();
		LinkedList<Integer>ages = new LinkedList<Integer>(19);
		names.addstart("Anna");
		names.addstart("Ricardos");
		names.addEnd("Jose Arturo");
		names.addEnd("Pedro");
		names.addEnd("Rodriguez");
		names.addEnd("Valles");
		ages.addstart(29);
		names.addBefore("Jose Arturo", "Alendro Orozco"); // <-- �-ERROR CRITICO-! (!YA FUNCIONO�)
		
		//ages.addBefore(19, 15); // <-- PUES NOSE QUE ONDA
		names.reindex();
		//names.pronter(); System.out.println("");
		//ages.list();
		
		names.replace("Pedro", "Aguirre"); // <-- Remplazamos nombre por otro
		//names.removeafter("Anna"); 
		//names.removebefore("Aguirre");
		names.reindex();
		names.list();
		names.removefirst();
		names.removelast();
		names.reindex();
		System.out.println("");
		names.list();
		System.out.println("");
		System.out.println("El tama�o de la lista es de: " + names.size());
		System.out.println("Encontrado en la posicion: " + names.indexof("Rodriguez"));
		names.getfirst();
		names.getlast();
		System.out.println("Se encontro...? " + names.buscar("Rodriguez"));
		names.isempty();
		names.eliminar();
		System.out.println("El tama�o de la lista es de: " + names.size());
		names.list();
		System.out.println("Se encontro...? " + names.buscar("Rodriguez"));
		names.isempty();
		System.gc();
	}

}