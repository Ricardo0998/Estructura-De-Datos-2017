package app;

import java.util.Random;

public class Test {

	    private Bolsa numbers;
	    private final static int SIZE = 1000;
	    private final static int MAX = 1001;

	    public void printNumbers(){
	    	numbers.print();
	    }
	    
	    public void setUp() {
	        numbers = new Bolsa(Integer.class,SIZE);
	        Random generator = new Random();
	        for (int i = 0; i < 1000; i++) {
	            numbers.add(generator.nextInt(MAX));
	 
	        }
	    }

	    public void testNull() {
	        Quicksort sorter = new Quicksort();
	        sorter.sort(null);
	    }

	    

	    public void testQuickSort() {
	    	//numbers.print();
	        long startTime = System.currentTimeMillis();

	        Quicksort sorter = new Quicksort();
	        sorter.sort(numbers);

	        long stopTime = System.currentTimeMillis();
	        long elapsedTime = stopTime - startTime;
	        System.out.println("Quicksort " + elapsedTime);
	        if (!validate(numbers)) {
	            System.out.println("Should not happen");
	        }
	        //assertTrue(true);
	    }
	    
	    public void testSeleccion() {
	    	long startTime = System.currentTimeMillis();

	        Seleccion select = new Seleccion();
	        select.sort(numbers);

	        long stopTime = System.currentTimeMillis();
	        long elapsedTime = stopTime - startTime;
	        System.out.println("Seleccion " + elapsedTime);
	        if (!validate(numbers)) {
	            System.out.println("Should not happen");
	        }
	    	
	    }


	    private boolean validate(Bolsa numbers) {
	        for (int i = 0; i < numbers.getLength() - 1; i++) {
	            if (numbers.get(i).compareTo(numbers.get(i+1)) == 1){
	                return false;
	            }
	        }
	        return true;
	    }

	    private void printResult(Bolsa numbers) {
	        for (int i = 0; i < numbers.getLength(); i++) {
	            System.out.print(numbers.get(i));
	        }
	        System.out.println();
	    }
	}

