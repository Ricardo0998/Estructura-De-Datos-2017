package App;

public class Converciones {

	static int resu = 0;
	static String res = "";
	static String numm = "";
	
    @SuppressWarnings("unused")
	public static String Convertir(int num, int base){
    	switch(base) {
    	case 2:
    			res = convertir2(num);
    			System.out.println(res);
    		break;
    	case 3:
    			res = convertir3(num);
				System.out.println(res);
    		break;
    	case 4:
    			res = convertir4(num);
				System.out.println(res);
    		break;
    	case 5:
    			res = convertir5(num);
				System.out.println(res);
    		break;
    	case 6:
    			res = convertir6(num);
				System.out.println(res);
    		break;
    	case 7:
    			res = convertir9(num);
				System.out.println(res);
    		break;
    	case 8:
    			res = convertir8(num);
    			System.out.println(res);
    		break;
    	case 9:
    			res = convertir9(num);
    			System.out.println(res);
    		break;
    	case 10:
				System.out.println(num);
    		break;
    	case 11:
    			numm = String.valueOf(num);
				res = convertir11(numm);
				System.out.println(res);
    		break;
    	case 12:
    			numm = String.valueOf(num);
				res = convertir12(numm);
				System.out.println(res);
    		break;
    	case 13:
    			numm = String.valueOf(num);
				res = convertir13(numm);
				System.out.println(res);
    		break;
    	case 14:
    			numm = String.valueOf(num);
				res = convertir14(numm);
				System.out.println(res);
    		break;
    	case 15:
    			numm = String.valueOf(num);
				res = convertir15(numm);
				System.out.println(res);
    		break;
    	case 16:
    			numm = String.valueOf(num);
    			res = convertir16(numm);
    			System.out.println(res);
    		break;
    	default:
    		System.out.println("Solo admite de la base 2 a la 16");
    		break;
    	}
    	
    	return "";
    }
    
    public static String convertir2(int n) {
    	int resu = n%2;
		if(n<=1){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/2)+resu;
		}
    }
    public static String convertir3(int n) {
    	int resu = n%3;
		if(n<=2){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/3)+resu;
		}
    }
    public static String convertir4(int n) {
    	int resu = n%4;
		if(n<=3){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/4)+resu;
		}
    }
    public static String convertir5(int n) {
    	int resu = n%5;
		if(n<=4){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/5)+resu;
		}
    }
    public static String convertir6(int n) {
    	int resu = n%6;
		if(n<=5){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/6)+resu;
		}
    }
    public static String convertir7(int n) {
    	int resu = n%7;
		if(n<=6){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/7)+resu;
		}
    }
    public static String convertir8(int n) {
    	int resu = n%8;
		if(n<=7){
			return ""+resu;
		}else{
			return ""+convertir8((n-resu)/8)+resu;
		}
    }
    public static String convertir9(int n) {
    	int resu = n%9;
		if(n<=8){
			return ""+resu;
		}else{
			return ""+convertir2((n-resu)/9)+resu;
		}
    }
    
    /****** SIGUE BUSCANDO LA BASE 10 CREEME ALGUN D�A LO ENCONTRARAS ******/
    
    public static String convertir11(String s) {
        String result = "";
        int n = Integer.parseInt(s);
        int remainder = n % 16;

        if (n == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                default:
                    result = remainder + result;
                    break;
            }
            return convertir16(Integer.toString(n / 16)) + result;
        }
    }
    public static String convertir12(String s) {
        String result = "";
        int n = Integer.parseInt(s);
        int remainder = n % 12;

        if (n == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                case 11:
                default:
                    result = remainder + result;
                    break;
            }
            return convertir16(Integer.toString(n / 12)) + result;
        }
    }
    public static String convertir13(String s) {
        String result = "";
        int n = Integer.parseInt(s);
        int remainder = n % 13;

        if (n == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                case 11:
                    result = "B";
                    break;
                case 12:
                    result = "C";
                    break;
                default:
                    result = remainder + result;
                    break;
            }
            return convertir16(Integer.toString(n / 13)) + result;
        }
    }
    public static String convertir14(String s) {
        String result = "";
        int n = Integer.parseInt(s);
        int remainder = n % 14;

        if (n == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                case 11:
                    result = "B";
                    break;
                case 12:
                    result = "C";
                    break;
                case 13:
                    result = "D";
                    break;
                default:
                    result = remainder + result;
                    break;
            }
            return convertir16(Integer.toString(n / 14)) + result;
        }
    }
    public static String convertir15(String s) {
        String result = "";
        int n = Integer.parseInt(s);
        int remainder = n % 15;

        if (n == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                case 11:
                    result = "B";
                    break;
                case 12:
                    result = "C";
                    break;
                case 13:
                    result = "D";
                    break;
                case 14:
                    result = "E";
                    break;
                default:
                    result = remainder + result;
                    break;
            }
            return convertir16(Integer.toString(n / 15)) + result;
        }
    }
    public static String convertir16(String s) {
        String result = "";
        int n = Integer.parseInt(s);
        int remainder = n % 16;

        if (n == 0) {
            return "";
        } else {
            switch (remainder) {
                case 10:
                    result = "A";
                    break;
                case 11:
                    result = "B";
                    break;
                case 12:
                    result = "C";
                    break;
                case 13:
                    result = "D";
                    break;
                case 14:
                    result = "E";
                    break;
                case 15:
                    result = "F";
                    break;
                default:
                    result = remainder + result;
                    break;
            }
            return convertir16(Integer.toString(n / 16)) + result;
        }
    }
	
}
