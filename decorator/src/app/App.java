package app;

public class App {

	public static void main(String[] args) {

		Vendible auto = new FiatUno();
		auto = new CdPlayer(auto);
		auto = new Gasolina(auto);

		System.out.println(auto.getDescripcion());
		System.out.println("El Precio Total es: " + auto.getPrecio());

		Vendible auto2 = new FordFiesta();
		auto2 = new CdPlayer(auto2);
		auto2 = new Mp3Player(auto2);
		auto2 = new Gasolina(auto2);
		auto2 = new AireAcondicionado(auto2);

		System.out.println("\n"+auto2.getDescripcion());
		System.out.println("El Precio Total es: " + auto2.getPrecio());
		
	/* 
	    -Es m�s flexible que la herencia: utilizando diferentes combinaciones de unos pocos
	    tipos distintos de objetos decorator, se puede crear muchas combinaciones distintas 
	    de comportamientos. Para crear esos diferentes tipos de comportamiento con la herencia
	    se requiere que definas muchas clases distintas.
	    
	    -Evita que las clases altas de la jerarqu�a est�n demasiado cargadas de funcionalidad.
	    
	    -Un componente y su decorador no son el mismo objeto.
	    
	    -Provoca la creaci�n de muchos objetos peque�os encadenados, lo que puede 
	    llegar a complicar la depuraci�n.
	    
	    -La flexibilidad de los objetos decorator los hace m�s propenso a errores que 
	    la herencia. Por ejemplo, es posible combinar objetos decorator de diferentes formas 
	    que no funcionen, o crear referencias circulares entre los objetos decorator.
	*/

	}

}