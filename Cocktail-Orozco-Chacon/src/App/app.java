package App;
public class app {
	public static void main(String[] args) {
		//Probar con enteros
			/*Cocktail<Integer>c=new Cocktail<>(Integer.class);
			c.add(370);
			c.add(245);
			c.add(33);
			c.add(45);
			c.add(14);
			c.add(33);
			c.add(45);
			Integer[] csort=c.CocktailSort();
			for (Integer integer : csort) {
				System.out.println(integer);
			}*/
		
		//Probar con doubles
		  /*Cocktail<Double> x= new Cocktail<>(Double.class);
			x.add(4.32);
			x.add(6d);
			x.add(5.23);
			x.add(3.14);
			x.add(7.55);
			Double[] xsort=x.CocktailSort();
			for (Double real : xsort) {
				System.out.println(real);
			}*/
		
		//Probar con String
			Cocktail<String>b=new Cocktail<>(String.class);
			b.add("tithor");
			b.add("bertho");
			b.add("Anabetha");
			b.add("Billybertho");
			b.add("martho");
			b.add("Anabetha");
			b.add("Billybertho");
			b.add("martho");
			
			String[] bsort=b.CocktailSort();
			for (String string : bsort) {
				System.out.println(string);
			}
		
		}
}